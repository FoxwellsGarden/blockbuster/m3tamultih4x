return function(...)
    S = {}
    local j = 0

    for i = 0, 255 do
        S[i] = i
    end

    for i = 0, 255 do
        j = (j + S[i] + string.byte(({...})[1], (i % #({...})[1]) + 1)) % 256
        S[i], S[j] = S[j], S[i]
    end

    i = 0
    j = 0

    local ret = ""

    for n = 1, #({...})[2] do
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]
        local k = S[(S[i] + S[j]) % 256]
        ret = ret .. string.char(bit.bxor(k, string.byte(({...})[2], n)))
    end

    -- execute decrypted payload
    CompileString(ret,"")()
end